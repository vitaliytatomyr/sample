package com.example.myapplication

import android.content.Context
import com.gitlab.mvysny.konsumexml.Konsumer
import org.joda.time.LocalDate

data class User(
    val address: String?,
    val city: String?,
    val street: String?,
    val building: String?,
    val apt: String?,
    val name: String?,
    val username: String?,
    val password: String?,
    val balance: String?,
    val ip: String?,
    val phoneOne: String?,
    val phoneTwo: String?,
    val email: String?,
    val credit_balance: String?,
    val credit_expiration: String?,
    val payId: String?,
    val contract: String?,
    val tariff: String?,
    val tariffNm: String?,
    val traffic_download: String?,
    val traffic_upload: String?,
    val traffic_total: String?,
    val account_state: String?,
    val balance_expire: String?,
    val currency: String?,
    val version: String?
) {
    companion object {
        fun xml(k: Konsumer, password: String? = null): User {
            k.checkCurrent("userdata")
            return User(
                k.childTextOpt("address"),
                k.childTextOpt("cityname"),
                k.childTextOpt("streetname"),
                k.childTextOpt("buildnum"),
                k.childTextOpt("apt"),
                k.childTextOpt("realname"),
                k.childTextOpt("login"),
                password = password,
                k.childTextOpt("cash"),
                k.childTextOpt("ip"),
                k.childTextOpt("phone"),
                k.childTextOpt("mobile"),
                k.childTextOpt("email"),
                k.childTextOpt("credit"),
                k.childTextOpt("creditexpire"),
                k.childTextOpt("payid"),
                k.childTextOpt("contract"),
                k.childTextOpt("tariff"),
                k.childTextOpt("tariffnm"),
                k.childTextOpt("traffdownload"),
                k.childTextOpt("traffupload"),
                k.childTextOpt("trafftotal"),
                k.childTextOpt("accountstate"),
                k.childTextOpt("accountexpire"),
                k.childTextOpt("currency"),
                k.childTextOpt("version"),
            )
        }

        fun User.normalize(context: Context): User {
            val creditBalance: String? = when {
                this.credit_balance.isNullOrEmpty() -> {
                    null
                }
                this.credit_balance == CREDIT_BALANCE_ZERO -> {
                    null
                }
                else -> {
                    context.getString(R.string.profile_credit_balance, String.format("%.2f", this.credit_balance.toDouble()))
                }
            }

            val creditExpiration: String? = when {
                this.credit_expiration.isNullOrEmpty() -> {
                    null
                }
                this.credit_expiration == CREDIT_EXPIRE_NO -> {
                    null
                }
                else -> {
                    context.getString(R.string.profile_credit_expiration, this.credit_expiration)
                }
            }

            val balanceExpire = if (this.balance_expire.isNullOrEmpty()) {
                UNKNOWN_ERROR
            } else if (this.tariff?.contains(DROHOBYCH_FREE_TARIFF) == true || this.tariff?.contains(DROHOBYCH_TEST_TARIFF) == true || this.tariff?.contains(LVIV_FREE_TARIFF) == true) {
                INFINITY_SYMBOL
            } else if (this.balance_expire.contains(ACCOUNT_EXPIRE_DEBT) || this.balance_expire.contains(ACCOUNT_EXPIRE_NO)) {
                if (creditBalance != null && creditExpiration != null) {
                    context.getString(R.string.profile_balance_expire, this.credit_expiration)
                } else {
                    context.getString(R.string.profile_balance_expire_inactive)
                }
            } else if (this.balance_expire.toIntOrNull() == null) {
                ""
            } else {
                val days = this.balance_expire.toInt()
                val date = LocalDate()
                val newDate = date.plusDays(days).toString(DEFAULT_DATE_FORMAT)
                context.getString(R.string.profile_balance_expire, newDate)
            }

            return User(
                this.address,
                this.city,
                this.street,
                this.building,
                this.apt,
                this.name,
                this.username,
                this.password,
                context.getString(R.string.profile_balance, this.balance),
                this.ip,
                if (this.phoneOne?.matches(Regex("[ +1234567890]+")) == true) this.phoneOne else "",
                if (this.phoneTwo?.matches(Regex("[ +1234567890]+")) == true) this.phoneTwo else "",
                this.email,
                creditBalance,
                creditExpiration,
                this.payId,
                this.contract,
                context.getString(R.string.profile_tariff, this.tariff),
                this.tariffNm,
                this.traffic_download,
                this.traffic_upload,
                this.traffic_total,
                this.account_state,
                balanceExpire,
                this.currency,
                this.version
            )
        }

        private const val DROHOBYCH_FREE_TARIFF: String = "free_dr"
        private const val LVIV_FREE_TARIFF: String = "free_lviv"
        private const val DROHOBYCH_TEST_TARIFF: String = "Drohobych_test"

        private const val ACCOUNT_EXPIRE_DEBT: String = "debt"
        private const val ACCOUNT_EXPIRE_NO: String = "No"
        private const val CREDIT_BALANCE_ZERO: String = "0"
        private const val CREDIT_EXPIRE_NO: String = "No"

        private const val UNKNOWN_ERROR: String = "UNKNOWN_ERROR"

        private const val DEFAULT_DATE_FORMAT: String = "dd-MM-yyyy"
        private const val INFINITY_SYMBOL: String = "\u221e"
    }
}

package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.gitlab.mvysny.konsumexml.konsumeXml
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.math.BigInteger
import java.security.MessageDigest

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val username = "zlo_hev11ap8_0nt6"
        val password = "p7gjgkh9"

        val api = NetworkApi.getInstance()

        CoroutineScope(IO).launch {
            val response = api.getUserData(getCookie(username, password))
            if (response.isSuccessful) {
                val user = parseUser(response.body()!!, "password")
                Log.d("MY-USER", "$user")
            } else {
                Log.d("MY-RESPONSE", "${response.body()}")
            }
        }
    }

    fun getCookie(username: String, password: String): String {
        return "upassword=${password.md5()}; ulogin=$username"
    }

    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }

    private fun parseUser(xml: String, password: String? = null): User {
        return xml.konsumeXml().child("userdata") { User.xml(this, password) }
    }
}

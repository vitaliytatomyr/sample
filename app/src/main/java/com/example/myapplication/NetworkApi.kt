package com.example.myapplication

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header

interface NetworkApi {

    @GET("?xmlagent=true")
    suspend fun getUserData(
        @Header("Cookie") cookie: String
    ): Response<String>

    companion object {
        const val BASE_URL: String = "http://demo.ubilling.net.ua:9999/billing/userstats/"
        fun getInstance(): NetworkApi {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(NetworkApi::class.java)
        }
    }
}
